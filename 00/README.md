# 00 UCU-CLOUD

## Facultad de Ingeniería y Tecnologías - Cloud Computing

**Profesores: [Federico Wagner](https://gitlab.com/u/visigoten) - [Rodolfo Pilas](https://gitlab.com/u/pilasguru)**

### 00 Prueba de instalación vagrant

En esta carpeta se dispone de un `Vagrantfile` básico para probar que vagrant y el sistema de virtulización funcionan correctamente.

Descargará y dejará disponible la imagen `maier/alpine-3.4-x86_64` para futuros usos.

#### Realizar los siguientes pasos

1. Ejecutar dentro de la carpeta `00/`

```
vagrant up
```

como se muestra en el siguiente video (del curso de LINUX):

[![asciicast](https://gitlab.com/pilasguru/UCU-LINUX/raw/master/docs/107964.png)](https://asciinema.org/a/107964)


2. Verificar aplicación con el navegador web

[http://localhost:8080](http://localhost:8080)


3. Verificar imágenes descargadas con el comando:

```
vagrant box list
```

que debe mostrar al menos:

```
maier/alpine-3.4-x86_64              (virtualbox, 1.0.0)
```

4. Acceder a la instancia

```
vagrant ssh
```

5. Borrar la máquina virtual creada

```
vagrant destroy
```

#### Otros comandos vagrant

* Gestión de máquina virtual con los siguientes comandos:
	* `vagrant status`
	* `vagrant halt` 
	* `vagrant stop`
	* `vagrant up`

### Ver además

* [Vagrant cheat-sheet](https://pi.lastr.us/doku.php?id=docs:virtualizacion:vagrant:vagrant_cheat_sheet)
* [Vagrant documentation](https://www.vagrantup.com/docs/index.html)
* [Vagrant Cloud](https://app.vagrantup.com/boxes/search) repositorio de _boxes_

### Libros recomendados

* [Vagrant cookbook](https://leanpub.com/vagrantcookbook)
* [Vagrant up and running](http://shop.oreilly.com/product/0636920026358.do)
