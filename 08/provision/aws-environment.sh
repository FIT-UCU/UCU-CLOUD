#!/bin/bash
USER="vagrant"
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get -y install awscli jq vagrant
if [ ! -d /home/$USER/.aws ]; then mkdir /home/$USER/.aws; fi
cp /vagrant/provision/credentials /home/$USER/.aws/
cat << EOF > /home/$USER/.aws/config
[default]
region = us-east-1
EOF
chown -R $USER:$USER /home/$USER/.aws
chmod 600 /home/$USER/.aws/*


ln -s /vagrant/vagrant-aws /home/vagrant/vagrant-aws
wget -q https://releases.hashicorp.com/vagrant/2.2.0/vagrant_2.2.0_x86_64.deb
dpkg -i vagrant_2.2.0_x86_64.deb
rm vagrant_2.2.0_x86_64.deb
runuser -l vagrant -c 'vagrant plugin install vagrant-aws'
runuser -l vagrant -c 'vagrant box add dummy https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box'
exit 0
