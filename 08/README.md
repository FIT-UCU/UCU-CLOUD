# 06 UCU-CLOUD

## Facultad de Ingeniería y Tecnologías - Cloud Computing

**Profesores: [Federico Wagner](https://gitlab.com/u/visigoten) - [Rodolfo Pilas](https://gitlab.com/u/pilasguru)**

### 06 - Vagrant para provision de aws


### Instalación

Copiar archivo `provision/credentials.orig` a `provision/credentials`

Editar el archivo `provision/credentials` y configurar los datos de acceso

```
[default]
aws_access_key_id = XXXXXXXXXXX
aws_secret_access_key = XXXXXXXXXXXXXXXXXXXXXXXX
```

Levantar el entorno de administración

```
vagrant up
```

Acceder (configurado para el usuario `vagrant`)

```
vagrant ssh
```

Probar entorno con alguno de estos comandos:

```
vagrant@aws:~$ aws ec2 describe-regions
{
    "Regions": [
        {
            "RegionName": "ap-south-1",
            "Endpoint": "ec2.ap-south-1.amazonaws.com"
        },
        …
    ]    
}
```

```
vagrant@aws:~$ aws ec2 describe-instances
{
    "Reservations": []
}
```

```
vagrant@aws:~$ aws ec2 describe-subnets
{
    "Subnets": [
        {
            "CidrBlock": "172.31.16.0/20",
            "State": "available",
            …
        }
    ]
}
```

```
vagrant@aws:~$ aws ec2 describe-key-pairs --output table
--------------------------------------------------------------
|                      DescribeKeyPairs                      |
+------------------------------------------------------------+
||                         KeyPairs                         ||
|+---------------------------------------+------------------+|
||             KeyFingerprint            |     KeyName      ||
|+---------------------------------------+------------------+|
||  d5:63:1e:19:e5:4f:e9:ce:e9:92:3d:0f: |  A-KEY-PAIR      ||
|+---------------------------------------+------------------+|
```

Al finalizar puede destruir el entorno (para volverlo a iniciar cuando necesite):

```
vagrant destroy -f
```

## Directorio vagrant-aws

En el directorio `~/vagrant-aws/` se dispone de un entono vagrant para levantar una instancia en aws.

Copiar `Vagrantfile.orig` a `Vagrantfile` y adecuar al entorno de instalación.



