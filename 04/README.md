# 01 UCU-CLOUD

## Facultad de Ingeniería y Tecnologías - Cloud Computing

**Profesores: [Federico Wagner](https://gitlab.com/u/visigoten) - [Rodolfo Pilas](https://gitlab.com/u/pilasguru)**

### 01 - AWS ECS (docker)

Instancia virtual para administración de servicios AWS con soporte docker y ejercicio ECS

*NOTA: esta instalación permite tener rápidamente disponible un entorno de administración de AWS, también es posible instalar las herramientas en el HOST (notebook) que utiliza, en cuyo caso no se necesita este entorno*

### Instalación

Editar el archivo `provision/credentials` y configurar los datos de acceso

```
[default]
aws_secret_access_key = XXXXXXXXXXXXXXXXXXXXXXXX
aws_access_key_id = XXXXXXXXXXX
```

Levantar el entorno de administración

```
vagrant up
```

Acceder (configurado para el usuario `vagrant`)

```
vagrant ssh
```

Probar entorno con alguno de estos comandos ejecutados como usuario *ubuntu*:

```
ubuntu@aws:~$ aws ec2 describe-regions
{
    "Regions": [
        {
            "RegionName": "ap-south-1",
            "Endpoint": "ec2.ap-south-1.amazonaws.com"
        },
        …
    ]    
}
```

```
ubuntu@aws:~$ aws ec2 describe-instances
{
    "Reservations": []
}
```

```
ubuntu@aws:~$ aws ec2 describe-subnets
{
    "Subnets": [
        {
            "CidrBlock": "172.31.16.0/20",
            "State": "available",
            …
        }
    ]
}
```

```
ubuntu@aws:~$ aws ec2 describe-key-pairs --output table
--------------------------------------------------------------
|                      DescribeKeyPairs                      |
+------------------------------------------------------------+
||                         KeyPairs                         ||
|+---------------------------------------+------------------+|
||             KeyFingerprint            |     KeyName      ||
|+---------------------------------------+------------------+|
||  d5:63:1e:19:e5:4f:e9:ce:e9:92:3d:0f: |  A-KEY-PAIR      ||
|+---------------------------------------+------------------+|
```

Al finalizar puede destruir el entorno:

```
vagrant destroy -f
```

### Referencias

* Tutorial de AWS Tutorial Series [https://youtu.be/kQBGbmrdYO4](https://youtu.be/kQBGbmrdYO4)




