#!/bin/bash

# Variables genericas de AWS que se pueden configurar 
# a) en el SCRIPT a continuacion (NO SUGERIDO!)
# export AWS_ACCESS_KEY_ID="key-id"
# export AWS_SECRET_ACCESS_KEY="secret_access_key"
# export AWS_DEFAULT_REGION="us-east-1"
# b) en un script aparte entorno del shell 
# c) utilizando la configuracion de la carpeta ~/.aws/ (SUGERIDO!) 
# export AWS_PROFILE=default  # y se puede utilizar otro profile

# Encuentra las instancias EC2 en estado 'running' que no tienen un nombre
instances_to_terminate=$(aws ec2 describe-instances \
    --filters "Name=instance-state-name,Values=running" \
    --query 'Reservations[].Instances[?!not_null(Tags[?Key==`Name`].Value)] | [].[InstanceId]' \
    --output text)

# Termina las instancias encontradas
if [ -n "${instances_to_terminate}" ]; then
    echo "Terminando las siguientes instancias sin nombre en estado 'running':"
    echo "${instances_to_terminate}"
    aws ec2 terminate-instances --instance-ids ${instances_to_terminate} --output table
else
    echo "No se encontraron instancias sin nombre en estado 'running'."
fi
