#!/bin/bash

# Crea una instancia en AWS
# Muestra el comando para ingresar por SSH vez queda levantada

# Variables genericas de AWS que se pueden configurar 
# a) en el SCRIPT a continuacion (NO SUGERIDO!)
# export AWS_ACCESS_KEY_ID="key-id"
# export AWS_SECRET_ACCESS_KEY="secret_access_key"
# export AWS_DEFAULT_REGION="us-east-1"
# b) en un script aparte entorno del shell 
# c) utilizando la configuracion de la carpeta ~/.aws/ (SUGERIDO!) 
# export AWS_PROFILE=default  # y se puede utilizar otro profile

# Incluir aqui el nombre del par de claves que se va a utilizar
KEY_NAME=pilas

# La instancia que se desea (t2.micro es capa gratis)
INSTANCE_TYPE="t2.micro"

# Amazon Machine Image (AMI) es dependiente de la region
AMI_ID="ami-0c8e23f950c7725b9" # us-east-1 - Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type 


####  ------- CODE ----

# Obtiene el default security group de la region
sg_default=$(aws ec2 describe-security-groups \
    --filters Name=group-name,Values=default \
    --query 'SecurityGroups[0].GroupId' \
    --output text)

# Crea una instancia EC2 y guarda su ID
# Instala y deja funcionando docker (Provision)
instance_id=$(aws ec2 run-instances \
    --image-id "${AMI_ID}" \
    --count 1 \
    --instance-type "${INSTANCE_TYPE}" \
    --key-name ${KEY_NAME} \
    --security-group-ids ${sg_default} \
    --user-data '#!/bin/bash
cd /opt
yum install -y git
git clone https://gitlab.com/pilasguru/user-data-provision.git
/bin/bash /opt/user-data-provision/amazonlinux/amzlnx.2023_docker_user-data' \
    --query 'Instances[0].InstanceId' \
    --output text)

echo "PROVIDER: Instancia EC2 creada: ${instance_id}"

# Espera hasta que la instancia esté en ejecución
echo "PROVIDER: Esperando a que la instancia EC2 esté en ejecución..."
aws ec2 wait instance-running --instance-ids "${instance_id}"

# Obtiene la IP pública de la instancia EC2
public_ip=$(aws ec2 describe-instances \
    --instance-ids "${instance_id}" \
    --query 'Reservations[0].Instances[0].PublicIpAddress' \
    --output text)

echo "PROVIDER: IP pública asignada: ${public_ip}"

# Agrega el puerto 22 al security group *default* 
aws ec2 authorize-security-group-ingress \
    --group-id $(aws ec2 describe-security-groups \
                --filters Name=group-name,Values=default \
                --query 'SecurityGroups[0].GroupId' \
                --output text) \
    --protocol tcp \
    --port 22 \
    --cidr 0.0.0.0/0 2>/dev/null

# Espera a que el puerto 22 esté abierto
echo "PROVISION: Esperando a que el puerto 22 esté abierto..."
while ! nc -z -v -w5 "${public_ip}" 22; do
    sleep 5
done

echo "PROVISION: La version del servidor docker instalado y corriendo es:"
while true; do 
    ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -q ec2-user@${public_ip} docker version --format '{{.Server.Version}}' && break
    sleep 5
    echo "Ctrl-C si el error persiste..."
done

echo "PROVISION: conectar a la instancia con el comando:"
echo "ssh -i ARCHIVO.pem ec2-user@${public_ip}"

