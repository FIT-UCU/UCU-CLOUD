# 01 UCU-CLOUD

## Facultad de Ingeniería y Tecnologías - Cloud Computing

**Profesore: [Rodolfo Pilas](https://gitlab.com/u/pilasguru)**

Este tutorial busca configurar los accesos para poder trabajar con las nubes de AWS y AZURE

Luego realizar una serie básica de usos con IaaS en ambas nubes

#### 1. [AWS](README-AWS.md)

#### 2. [AZURE](README-AZURE.md)

