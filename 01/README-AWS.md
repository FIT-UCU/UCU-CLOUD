
# AWS basic configuration

## Configurar usuario no-root

### Crear usuario no-root

- Ingresar como usuario ROOT de AWS
- Configurar en el servicio IAM un usuario nuevo
  * Usuario IAM
  * Adjuntar politica AdministratorAccess
- En el usuario creado, Credenciales de Seguridad crear una Clave de Acceso

Guardar:

* URL de acceso
* API_KEY
* SECRET_KEY

### Configurar EC2 para usuario no-root

- Ingresar como el usuario no-root creado en el paso anterior por la URL de la organziación
- En el servicio EC2 crear o subir una nuevo `Par de Claves` con algoritmo *ed25519*


### Configurar AWS command line interface


Administración de AWS mediante CLI.

```
$ aws configure


$ cat ~/.aws/config
[profile default]
region = us-east-1
cli_pager =

$ cat ~/.aws/credentials
[default]
aws_access_key_id = AKXXXXXXXXXXXXXXXXFG
aws_secret_access_key = UOj6XXXXXXXXXXXXXXXXM2
```

Check:
```
$  aws sts get-caller-identity
{
    "UserId": "KXXXXXXXXXXXXXXXXMA",
    "Account": "58555555567",
    "Arn": "arn:aws:iam::58555555567:user/ucu-user"
}
```


### Comandos basicos

```
$ aws ec2 describe-regions --query "Regions[*].RegionName"
[
    "ap-south-1",
    "eu-north-1",
    "eu-west-3",
    "eu-west-2",
    "eu-west-1",
    "ap-northeast-3",
    "ap-northeast-2",
    "ap-northeast-1",
    "ca-central-1",
    "sa-east-1",
    "ap-southeast-1",
    "ap-southeast-2",
    "eu-central-1",
    "us-east-1",
    "us-east-2",
    "us-west-1",
    "us-west-2"
]

$ aws ec2 describe-instances
{
    "Reservations": []
}


$ aws ec2 describe-subnets
{
    "Subnets": [
        {
            "CidrBlock": "172.31.16.0/20",
            "State": "available",
            …
        }
    ]
}


$ aws ec2 describe-key-pairs --output table
--------------------------------------------------------------
|                      DescribeKeyPairs                      |
+------------------------------------------------------------+
||                         KeyPairs                         ||
|+---------------------------------------+------------------+|
||             KeyFingerprint            |     KeyName      ||
|+---------------------------------------+------------------+|
||  d5:63:1e:19:e5:4f:e9:ce:e9:92:3d:0f: |  A-KEY-PAIR      ||
|+---------------------------------------+------------------+|

```

### Crear una instancia


#### Crear una instancia por consola

- Hacer un checklist de las decisiones y recursos que se van tomando


#### Crear una instancia con comando aws

```
$ aws ec2 run-instances
```


