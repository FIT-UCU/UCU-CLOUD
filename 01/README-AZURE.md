
# AZURE basic configuration

## Crear grupo de recursos

- Crear un grupo de resucros dentro de la suscripción

## Crear una clave SSH

- Cear una clave SSH con algoritmo *ed25519* en el grupo de recursos.

### Acceder mediante CLI

Acceso
```
$ az login
To sign in, use a web browser to open the page https://microsoft.com/devicelogin and enter the code F3ZSM5465 to authenticate


az account show
{
  "environmentName": "AzureCloud",
  "homeTenantId": "e0f8e163-817a-8888-b88e-7b4698fe54",
  "id": "55d90033-8777-485e-8c56-db1a383f71",
  "isDefault": true,
  "managedByTenants": [],
  "name": "Azure for Students",
  "state": "Enabled",
  "tenantDefaultDomain": "correoucuedu.onmicrosoft.com",
  "tenantDisplayName": "Universidad Católica del Uruguay",
  "tenantId": "e0f33163-817a-4556-b88e-7b4e5425",
  "user": {
    "name": "RP@ucu.edu.uy",
    "type": "user"
  }
}
```


Comandos basicos

```
$ az account list-locations --query '[].name'
[
  "eastus",
  "southcentralus",
  "westus2",
  "westus3",
  "australiaeast",
...


$ az vm list --show-details
[]


$ az network vnet list
[]


$ az sshkey list --output table
Location    Name    PublicKey                                                                                            ResourceGroup
----------  ------  ---------------------------------------------------------------------------------------------------  ---------------
eastus      test1   ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMLDmhaRiXtesRTVeCZ8d5moMXzhwlZjCg0NvG7Lr4Qt generated-by-azure  UCU-2024-RG

```

### Crear una instancia


#### Crear una instancia por consola


#### Crear una instancia con comando aws

```
$ az vm create
```


