# UCU-CLOUD
![UCU 30](http://www.ucu.edu.uy/sites/all/themes/univer/logo.png)
## Facultad de Ingeniería y Tecnologías
### Materia: Cloud Computing 

**Profesor: [Federico Wagner](https://gitlab.com/u/visigoten)**

**Profesor: [Rodolfo Pilas](https://gitlab.com/u/pilasguru)**

Este conjunto de archivos son necesarios para levantar la infraestructura (virtual) requerida para seguir las diferentes etapas del curso.

### Requisitos previos

En términos generales, se requiere disponer de estos componentes instalados localmente:

1. Sistema de Virtualización: Oracle Virtualbox 
2. Software de proveer y aprovisionamiento de instancias virtuales: Vagrant
3. Cliente de repositrio GIT (comando `git`)
4. Cliente para acceso Secure Shell: OpenSSH o Git-bash
5. Algunas imágenes para proveer máquinas virtuales: vagrant box (se descargan cuando son necesarias)
6. Algunos complementos (plugin) de vagrant (se descargan cuando son necesarios)

Sobre el hardware es preferible disponer de un procesador 64bits con las extensiones de virtualización activadas y al menos 1GB de RAM. 

*NOTA:* En caso de no disponer de un procesador con extensiones de virtualizacón activas, el virtualizador (Virtualbox) solo levantará máquinas virtuales
de 32bits (aunque el procesador real sea de 64bits), por lo que las imágenes (boxes) deben ser cambiadas por equivalentes en 32bits.

#### Software para Windows

* [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* VirtualBox X.X.XX Oracle VM [VirtualBox Extension Pack](https://www.virtualbox.org/wiki/Downloads)
* Git-Bash <https://msysgit.github.io/>
* [Vagrant](https://www.vagrantup.com/downloads.html)

Video explicativo en Youtube:
[![Vagrant on Windows](https://i.ytimg.com/vi/NBA45jh374I/hqdefault.jpg)](https://www.youtube.com/watch?v=NBA45jh374I)

#### Software para Linux

* Del repositorio instalar git para la distribución que se usa
* [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* VirtualBox X.X.XX Oracle VM [VirtualBox Extension Pack](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)

(se sugiere versiones descargadas de la página, pues paquetes suelen ser versiones más antiguas)

Video explicativo en Youtube:
[![Vagrant en Ubuntu](https://i.ytimg.com/vi/yGviTwD3hWM/hqdefault.jpg)](https://www.youtube.com/watch?v=yGviTwD3hWM)

#### Sobre sistema MacOSX

* [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* VirtualBox X.X.XX Oracle VM [VirtualBox Extension Pack](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)

(se sugiere versiones descargadas de la página, en lugar de gestor de paquetes)

Video explicativo en Youtube:
[![Vagrant en MacOS](https://i.ytimg.com/vi/EsN4lnUp4is/hqdefault.jpg)](https://www.youtube.com/watch?v=EsN4lnUp4is)

#### Imagenes máquinas virtuales (boxes)

La *primera vez* que inicie una máquina virtual con una imagen nueva (que no tenga descargada) el sistema la descargará y demorará de acuerdo a su conexión.

### Instalación, verificación y primer prueba

- Instalar software previo
- (en Windows): Agregar al PATH el directorio *bin* de Git-bash, semejante a: C:\Program Files\Git-Bash\bin (confirmar en cada instalación)
- Verificar instalación con el comando

```
vagrant —version
```

- Clonar este repositorio en una carpeta de trabajo

```
git clone https://gitlab.com/FIT-UCU/UCU-CLOUD.git
```

- Ingresar en el directorio *UCU-CLOUD/00/* y ejecutar

```
vagrant up
```

- Cuando finalice conectar con el navegador a

```
http://localhost:8080/
```

### Estructura de red para ejercicios en clase

La estructura virtual que queda formada (con ruteos y enmascaramientos para la navegación) es:

![Network](https://gitlab.com/FIT-UCU/UCU-CLOUD/raw/master/docs/UCU-CLOUD.png)

Estas máquinas virtuales que son levantadas

* router
* lamp

tienen software que se provee automáticamente y el que no puede ser proveído se dejan disponibles distintos archivos para utilizar en el curso.


