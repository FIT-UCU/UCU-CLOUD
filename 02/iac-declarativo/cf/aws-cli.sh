

aws cloudformation deploy \
   --stack-name scale-my-service-name \
   --template-file scale-service-by-cpu.yml \
   --capabilities CAPABILITY_IAM \
   --parameter-overrides ClusterName=development ServiceName=my-web-service


aws cloudformation delete-stack --stack-name scale-my-service-name


