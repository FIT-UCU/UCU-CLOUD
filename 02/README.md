# Infraestructure as Code

Este codigo es un ejemplo de levantar una instancia en AWS utilizando `aws cli` en Bash scripting, `terraform` y `CloudFormation`.

Para que estos ejemplos es necesario:

* `aws cli` y `terraform`
* Configuración de acceso AWS (`aws configure`)
* Configurar los datos correspondientes a sub-red y keypair correspondientes a los archivos de IaC donde se va a desplegar la instancia.

```
ATENCION: Los script son basicos y pueden funcionar de forma no deseada
en infraestructuras con otros despliegues. Se recomienta el uso exclusivo 
en ambientes de pruebas.
```

## Proceso
Repetir este proceso con cada tipo de sistema (`aws cli`, `cloudformation` y `terraform`)

1. En la nube solo tener configurado:
  * KeyPair
  * Default Security Group
2. Crear infraestructura con IaC
  * Security Group
  * Instancia con servidor web y página que informa hostname
3. Prueba acceso 
4. Verificar recursos
5. Mejorar infraestructura con IaC
  * ElasticIP para la instancia
7. Prueba acceso
8. Verificar recursos
9. Borrar todo lo instalado (terminar, limpiar)


