# Infraestructure as Code - CloudFormation

Esta es una serie de ejercicios para realizar con CloudFormation para entender el funcionamiento de Infraestructura como Código

Ref: [What is AWS Cloudformation?](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html)

Ref: [Resourse Reference](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html)

## 01-Resources/01-instance.yaml

Stack básico que levanta dos Resources:

- AWS::EC2::Instance
- AWS::EC2::SecurityGroup

## 01-Resources/02-user-data.yaml

Contiene `Property: UserData:` para crear el sitio web. (Requiere ajustar SG)

## 01-Resources/03-elasticIP.yaml

Configura una IP Elastica associada a la instancia

## 02-Parameters/01-instance.yaml

Muestra el uso de parámetros para un stack Cloudformation

## Example

[Panel to deploy web application](https://pilasguru-sharedfiles.s3.amazonaws.com/deploy-instance-web.html)

:x